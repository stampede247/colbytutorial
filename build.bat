@echo off

call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64

set TimeString=%date:~-4,4%%date:~-10,2%%date:~-7,2%%time:~0,2%%time:~3,2%%time:~6,2%
set ProjectName=ColbyTutorial
set DebugBuild=1

echo Building %ProjectName% in %cd%

if "%DebugBuild%"=="1" (
	set DebugDependantFlags=/MTd -DDEBUG=1
	set DebugDependantPaths=/LIBPATH:"..\lib\debug"
	set DebugDependantLibraries=glew32d.lib
) else (
	set DebugDependantFlags=/MT -DDEBUG=0
	set DebugDependantPaths=/LIBPATH:"..\lib\release"
	set DebugDependantLibraries=glew32.lib
)

set CompilerFlags=/Zi /EHsc /nologo /FC %DebugDependantFlags%
set LinkerFlags=-incremental:no
set IncludeDirectories=/I..\..\..\lib\glfw\include /I..\..\..\lib\glew\include /I..\..\..\lib\mylib /I..\..\..\lib\stb
set LibraryDirectories=%DebugDependantPaths%
set Libraries=glfw3.lib gdi32.lib User32.lib Shell32.lib opengl32.lib glfw3.lib Shlwapi.lib %DebugDependantLibraries%
set ExecutableName=Colby.exe
set MainCppFile=win32_main.cpp

echo [Compiling %MainCppFile%]

cl /Fe%ExecutableName% %CompilerFlags% %IncludeDirectories% ..\code\%MainCppFile% /link %LibraryDirectories% %LinkerFlags% %Libraries%
