/*
File:   app_helpers.h
Author: Taylor Robbins
Date:   09\29\2017
Description:
	** Holds some useful functions and macros that the whole application can use
*/

#ifndef _APP_HELPERS_H
#define _APP_HELPERS_H

#define ButtonPressed(button)  ((input->buttons[button].isDown && input->buttons[button].transCount > 0) || input->buttons[button].transCount >= 2)
#define ButtonReleased(button) ((!input->buttons[button].isDown && input->buttons[button].transCount > 0) || input->buttons[button].transCount >= 2)
#define ButtonDown(button)     (input->buttons[button].isDown)

#endif //  _APP_HELPERS_H
