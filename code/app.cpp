/*
File:   app.cpp
Author: Taylor Robbins
Date:   09\29\2017
Description: 
	** Holds the top layer code for the application. We also #include all application source code files
*/

//External dependencies
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"
//mylib helpers
#include "memoryArena.h"
#include "tempMemory.h"
#include "tempMemory.cpp"
#include "colors.h"

// +--------------------------------------------------------------+
// |                   Application Header Files                   |
// +--------------------------------------------------------------+
#include "appStructs.h"
#include "appHelpers.h"
#include "appRenderState.h"

// +--------------------------------------------------------------+
// |                  Application Public Globals                  |
// +--------------------------------------------------------------+
struct AppData_t
{
	MemoryArena_t heapArena;
	MemoryArena_t tempArena;
	
	VertexArray_t vertArray;
	
	Shader_t simpleShader;
	Texture_t dotTexture;
	Texture_t testTexture;
	Texture_t testSprite;
	VertexBuffer_t squareVertBuffer;
	
	Color_t backgroundColor;
	
	RenderState_t renderState;
};

AppData_t* app = nullptr;
MemoryArena_t* mainHeap = nullptr;
AppInput_t* input = nullptr;

// +--------------------------------------------------------------+
// |                   App Source Code Includes                   |
// +--------------------------------------------------------------+
#include "appLoadingFunctions.cpp"
#include "appRenderState.cpp"

// +--------------------------------------------------------------+
// |                       Helper Functions                       |
// +--------------------------------------------------------------+


// +--------------------------------------------------------------+
// |                      App Initialization                      |
// +--------------------------------------------------------------+
void AppInit(AppMemory_t* appMemory)
{
	INFO_WriteLine("App initializing...");
	
	// +====================================+
	// | Allocate AppData and MemoryArenas  |
	// +====================================+
	Assert(sizeof(AppData_t) <= appMemory->mainSize);
	app = (AppData_t*)appMemory->main;
	
	u8* heapBase = (u8*)(app+1);
	u32 heapSize = appMemory->mainSize - sizeof(AppData_t);
	InitializeMemoryArenaHeap(&app->heapArena, heapBase, heapSize);
	mainHeap = &app->heapArena;
	
	InitializeMemoryArenaTemp(&app->tempArena, appMemory->temp, appMemory->tempSize, 32);
	TempArena = &app->tempArena;
	
	app->simpleShader = LoadShader("Resources/Shaders/simple-vertex.glsl", "Resources/Shaders/simple-fragment.glsl");
	app->testTexture = LoadTexture("Resources/Sprites/rotational_media.jpg");
	app->testSprite = LoadTexture("Resources/Sprites/greeter.gif");
	u32 dotTextureData = 0xFFFFFFFF;
	app->dotTexture = CreateTexture((u8*)&dotTextureData, 1, 1);
	
	app->vertArray = CreateVertexArray();
	
	// +==============================+
	// |        Create the VBO        |
	// +==============================+
	{
		const Vertex_t vertices[6] =
		{
			{ {0, 0, 0}, {1, 1, 1, 1}, {0, 0} },
			{ {1, 0, 0}, {1, 1, 1, 1}, {1, 0} },
			{ {0, 1, 0}, {1, 1, 1, 1}, {0, 1} },
			
			{ {0, 1, 0}, {1, 1, 1, 1}, {0, 1} },
			{ {1, 0, 0}, {1, 1, 1, 1}, {1, 0} },
			{ {1, 1, 0}, {1, 1, 1, 1}, {1, 1} },
		};
		app->squareVertBuffer = CreateVertexBuffer(vertices, ArrayCount(vertices));
	}
	
	InitializeRenderState(&app->renderState);
	
	INFO_WriteLine("App initialization done!");
}

// +--------------------------------------------------------------+
// |                    App Update and Render                     |
// +--------------------------------------------------------------+
void AppUpdateAndRender(AppMemory_t* appMemory, AppInput_t* appInput)
{
	app = (AppData_t*)appMemory->main;
	mainHeap = &app->heapArena;
	TempArena = &app->tempArena;
	input = appInput;
	
	TempPushMark();
	
	// +--------------------------------------------------------------+
	// |                            Update                            |
	// +--------------------------------------------------------------+
	if (ButtonPressed(Button_Enter))
	{
		DEBUG_WriteLine("Changing background color");
		
		if (app->backgroundColor.value == Color_Orange)
		{
			app->backgroundColor.value = Color_Turquoise;
		}
		else
		{
			app->backgroundColor.value = Color_Orange;
		}
	}
	
	// +--------------------------------------------------------------+
	// |                            Render                            |
	// +--------------------------------------------------------------+
	Shader_t* boundShader = nullptr;
	glClearColor(app->backgroundColor.r/255.f, app->backgroundColor.g/255.f, app->backgroundColor.b/255.f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	
	// +==============================+
	// |    Set up the RenderState    |
	// +==============================+
	RenderState_t* rs = &app->renderState;
	BindShader(rs, &app->simpleShader);
	BindVertexBuffer(rs, &app->squareVertBuffer);
	SetWorldMatrix(rs, Matrix4_Identity);
	Matrix4_t viewMatrix = Matrix4_Identity;
	SetViewMatrix(rs, viewMatrix);
	Matrix4_t projectionMatrix = Matrix4Scale(NewVec3(2/(r32)screenWidth, -2/(r32)screenHeight, 1));
	projectionMatrix = Matrix4Multiply(projectionMatrix, Matrix4Translate(NewVec3(-screenWidth/2.f, -screenHeight/2.f, 0.0f)));
	SetProjectionMatrix(rs, projectionMatrix);
	
	BindTexture(rs, &app->testTexture);
	DrawSprite(rs, NewRectangle(input->mousePos.x, input->mousePos.y, 100, 100), {Color_White});
	DrawSprite(rs, NewRectangle(0, input->mousePos.y, 200, 100),                 {Color_White});
	DrawSprite(rs, NewRectangle(input->mousePos.x, 0, 100, 20),                  {Color_White});
	
	BindTexture(rs, &app->testSprite);
	DrawSprite(rs, NewRectangle(screenWidth - input->mousePos.x, input->mousePos.y, 100, 100), {Color_White});
	
	DrawCircle(rs, NewVec2(100, 100), 100, {Color_Red});
	
	glUseProgram(0); boundShader = nullptr;
	
	TempPopMark();
}

