/*
File:   main.cpp
Author: Taylor Robbins
Date:   09\29\2017
Description: 
	** Holds the main entry point for the program. Includes all other files that need to be compiled 
*/

#include <stdio.h>
#include <windows.h>
#include <math.h>
#include <string.h>
#include <stdint.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

//These come from mylib
#define USE_ASSERT_FAILURE_FUNCTION true
#include "win32_assert.h"
#include "win32_defines.h"
#include "win32_intrinsics.h"
#include "myMath.h"

// +--------------------------------------------------------------+
// |                        Public Defines                        |
// +--------------------------------------------------------------+
#define OPEN_CONSOLE_WINDOW  true
#define WINDOW_WIDTH         800
#define WINDOW_HEIGHT        600
#define WINDOW_TITLE         "GLFW Window"
#define WINDOW_RESIZABLE     true

#define TOPMOST_WINDOW       DEBUG //only if in debug mode
#define DEPTH_BUFFER_BITS    8
#define STENCIL_BUFFER_BITS  8
#define ANTIALIASING_SAMPLES 0

#define APPLICATION_MEMORY_SIZE Kilobytes(10)
#define APPLICATION_TEMP_MEMORY_SIZE Kilobytes(10)

// +--------------------------------------------------------------+
// |              Public Structure/Type Definitions               |
// +--------------------------------------------------------------+
struct AppMemory_t
{
	void* main;
	u32 mainSize;
	void* temp;
	u32 tempSize;
};
#include "win32_input.h"
#include "win32_files.h"

// +--------------------------------------------------------------+
// |                        Public Globals                        |
// +--------------------------------------------------------------+
GLFWwindow* window = nullptr;
AppInput_t* lastAppInput = nullptr;
AppInput_t* currentAppInput = nullptr;
i32 screenWidth    = 0;
i32 screenHeight   = 0;

// +--------------------------------------------------------------+
// |                     Inlude Source Files                      |
// +--------------------------------------------------------------+
#include "win32_debug.cpp"
#include "win32_input.cpp"
#include "win32_glfwCallbacks.cpp"
#include "win32_files.cpp"

//NOTE: app.cpp will #include all of the application source code
#include "app.cpp"

// +--------------------------------------------------------------+
// |                      Private Functions                       |
// +--------------------------------------------------------------+
void GlfwErrorCallback(i32 errorCode, const char* description)
{
	ALERT_PrintLine("GLFW Error: (%d) %s", errorCode, description);
}

// +--------------------------------------------------------------+
// |                   Windows Main Entry Point                   |
// +--------------------------------------------------------------+
#if OPEN_CONSOLE_WINDOW
int main()
#else
int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int nCmdShow)
#endif
{
	INFO_WriteLine("Program Starting...");
	
	// +==============================+
	// |     GLFW Initialization      |
	// +==============================+
	glfwSetErrorCallback(GlfwErrorCallback);
	i32 glVersionMajor, glVersionMinor, glVersionRevision;;
	glfwGetVersion(&glVersionMajor, &glVersionMinor, &glVersionRevision);
	DEBUG_PrintLine("OpenGL Version %d.%d(%d)", glVersionMajor, glVersionMinor, glVersionRevision);
	
	DEBUG_WriteLine("Initializing GLFW...");
	if (glfwInit() == false)
	{
		ALERT_WriteLine("glfwInit failed!");
		Assert(false);
		return 1;
	}
	
	// +==============================+
	// |       Window Creation        |
	// +==============================+
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_CLIENT_API,            GLFW_OPENGL_API);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, false); //Makes MacOSX happy?
	glfwWindowHint(GLFW_OPENGL_PROFILE,        GLFW_OPENGL_ANY_PROFILE);//GLFW_OPENGL_CORE_PROFILE
	
	glfwWindowHint(GLFW_RESIZABLE,    WINDOW_RESIZABLE);
	glfwWindowHint(GLFW_FLOATING,     TOPMOST_WINDOW);
	glfwWindowHint(GLFW_DECORATED,    true);
	glfwWindowHint(GLFW_FOCUSED,      true);
	glfwWindowHint(GLFW_DOUBLEBUFFER, true);
	glfwWindowHint(GLFW_RED_BITS,     8);
	glfwWindowHint(GLFW_GREEN_BITS,   8);
	glfwWindowHint(GLFW_BLUE_BITS,    8);
	glfwWindowHint(GLFW_ALPHA_BITS,   8);
	glfwWindowHint(GLFW_DEPTH_BITS,   DEPTH_BUFFER_BITS);
	glfwWindowHint(GLFW_STENCIL_BITS, STENCIL_BUFFER_BITS);
	glfwWindowHint(GLFW_SAMPLES,      ANTIALIASING_SAMPLES);
	
	DEBUG_WriteLine("Creating GLFW window...");
	window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE, NULL, NULL);
	if (window == nullptr)
	{
		ALERT_WriteLine("glfwCreateWindow failed!");
		Assert(false);
		glfwTerminate();
		return 1;
	}
	
	glfwMakeContextCurrent(window);
	
	glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
	glViewport(0, 0, screenWidth, screenHeight);
	DEBUG_PrintLine("Screen Size: %dx%d", screenWidth, screenHeight);
	glfwSwapInterval(1);
	
	// +==============================+
	// |   Register GLFW Callbacks    |
	// +==============================+
	glfwSetWindowCloseCallback(window,     GlfwWindowCloseCallback);
	glfwSetFramebufferSizeCallback(window, GlfwWindowSizeCallback);
	glfwSetWindowPosCallback(window,       GlfwWindowMoveCallback);
	glfwSetWindowIconifyCallback(window,   GlfwWindowMinimizeCallback);
	glfwSetKeyCallback(window,             GlfwKeyPressedCallback);
	glfwSetCharCallback(window,            GlfwCharPressedCallback);
	glfwSetCursorPosCallback(window,       GlfwCursorPosCallback);
	glfwSetMouseButtonCallback(window,     GlfwMousePressCallback);
	glfwSetScrollCallback(window,          GlfwMouseScrollCallback);
	
	// +==============================+
	// |     GLEW Initialization      |
	// +==============================+
	glewExperimental = GL_TRUE;
	DEBUG_WriteLine("Initializing GLEW...");
	GLenum glewInitError = glewInit();
	if (glewInitError != GLEW_OK)
	{
		ALERT_WriteLine("glewInit failed!");
		Assert(false);
		glfwTerminate();
		return 1;
	}
	
	const GLubyte* glVersionString = glGetString(GL_VERSION);
	const GLubyte* graphicsCardString = glGetString(GL_RENDERER);
	const GLFWvidmode* glfwModePntr = glfwGetVideoMode(glfwGetPrimaryMonitor());
	DEBUG_PrintLine("Opengl Version:  %s", glVersionString);
	DEBUG_PrintLine("Graphics Card:   %s", graphicsCardString);
	DEBUG_PrintLine("Monitor Refresh: %dHz", glfwModePntr->refreshRate);
	
	// +==============================+
	// |  Application Initialization  |
	// +==============================+
	AppMemory_t appMemory = {};
	appMemory.mainSize = APPLICATION_MEMORY_SIZE;
	appMemory.main = (void*)VirtualAlloc(0, appMemory.mainSize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
	appMemory.tempSize = APPLICATION_TEMP_MEMORY_SIZE;
	appMemory.temp = (void*)VirtualAlloc(0, appMemory.tempSize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
	
	AppInit(&appMemory);
	
	AppInput_t appInputArray[2] = {};
	currentAppInput = &appInputArray[0];
	lastAppInput = &appInputArray[1];
	
	// +==============================+
	// |    Main Application Loop     |
	// +==============================+
	INFO_WriteLine("Entering main loop");
	while (glfwWindowShouldClose(window) == false)
	{
		// +==============================+
		// |     Handle Window Events     |
		// +==============================+
		glfwPollEvents();
		glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
		glViewport(0, 0, screenWidth, screenHeight);
		
		// +==============================+
		// |    Update the ProgramTime    |
		// +==============================+
		u64 lastTime = currentAppInput->programTime;
		currentAppInput->programTime = (u64)(glfwGetTime() * 1000);
		currentAppInput->timeDelta = (r64)(currentAppInput->programTime - lastTime) / (1000 / glfwModePntr->refreshRate);
		
		// +==============================+
		// |    App Update and Render     |
		// +==============================+
		AppUpdateAndRender(&appMemory, currentAppInput);
		
		glfwSwapBuffers(window);
		
		// +==============================+
		// |    Swap AppInput pointers    |
		// +==============================+
		AppInput_t* tempAppInputPntr = currentAppInput;
		currentAppInput = lastAppInput;
		lastAppInput = tempAppInputPntr;
		*currentAppInput = *lastAppInput;
		ResetAppInput(currentAppInput);
	}
	ALERT_WriteLine("Exiting main loop!");
	
	glfwTerminate();
	ALERT_WriteLine("Program finished.");
	return 0;
}

#if DEBUG
void AssertFailure(const char* function, const char* filename, int lineNumber, const char* expressionStr)
{
	ALERT_PrintLine("Assertion failed in %s line %d function %s:", filename, lineNumber, function);
	ALERT_PrintLine("Assert(%s)", expressionStr);
}
#endif
