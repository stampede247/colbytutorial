/*
File:   win32_files.cpp
Author: Taylor Robbins
Date:   09\30\2017
Description: 
	** Holds all the functions that manage reading and writing files on windows 
*/

void Win32_FreeFileMemory(FileInfo_t* fileInfo)
{
	if(fileInfo->content != nullptr)
	{
		VirtualFree(fileInfo->content, 0, MEM_RELEASE);
		fileInfo->content = nullptr;
	}
}

FileInfo_t Win32_ReadEntireFile(const char* filename)
{
	FileInfo_t result = {};
	
	HANDLE fileHandle = CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
	if(fileHandle != INVALID_HANDLE_VALUE)
	{
		LARGE_INTEGER fileSize;
		if(GetFileSizeEx(fileHandle, &fileSize))
		{
			//TODO: Define and use SafeTruncateUInt64 
			uint32 fileSize32 = (uint32)(fileSize.QuadPart);
			result.content = VirtualAlloc(0, fileSize32, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
			if(result.content)
			{
				DWORD bytesRead;
				if(ReadFile(fileHandle, result.content, fileSize32, &bytesRead, 0) &&
				   (fileSize32 == bytesRead))
				{
					// NOTE: File read successfully
					result.size = fileSize32;
				}
				else
				{
					Win32_FreeFileMemory(&result);
					result.content = 0;
				}
			}
			else
			{
				// TODO: Logging
			}
		}
		else
		{
			// TODO: Logging
		}

		CloseHandle(fileHandle);
	}
	else
	{
		// TODO(casey): Logging
	}

	return result;
}

bool32 Win32_WriteEntireFile(const char* filename, void* memory, u32 memorySize)
{
	bool32 result = false;
	
	HANDLE fileHandle = CreateFileA(
		filename,              //Name of the file
		GENERIC_WRITE,         //Open for writing
		0,                     //Do not share
		NULL,                  //Default security
		CREATE_ALWAYS,         //Always overwrite
		FILE_ATTRIBUTE_NORMAL, //Default file attributes
		0                      //No Template File
	);
	if(fileHandle != INVALID_HANDLE_VALUE)
	{
		DWORD bytesWritten;
		if (WriteFile(fileHandle, memory, memorySize, &bytesWritten, 0))
		{
			// NOTE: File read successfully
			result = (bytesWritten == memorySize);
		}
		else
		{
			// TODO: Logging
		}

		CloseHandle(fileHandle);
	}
	else
	{
		// TODO: Logging
	}

	return result;
}

bool32 Win32_OpenFile(const char* filename, OpenFile_t* openFileOut)
{
	Assert(openFileOut != nullptr);
	Assert(filename != nullptr);
	
	HANDLE fileHandle = CreateFileA(
		filename,              //Name of the file
		GENERIC_WRITE,         //Open for reading and writing
		FILE_SHARE_READ,       //Do not share
		NULL,                  //Default security
		OPEN_ALWAYS,           //Open existing or create new
		FILE_ATTRIBUTE_NORMAL, //Default file attributes
		NULL                   //No Template File
	);
	
	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		DEBUG_PrintLine("Couldn't open/create file: \"%s\"", filename);
		return false;
	}
	
	SetFilePointer(fileHandle, 0, NULL, FILE_END);
	
	openFileOut->isOpen = true;
	openFileOut->handle = fileHandle;
	return true;
}

bool32 Win32_AppendFile(OpenFile_t* filePntr, const void* newData, u32 newDataSize)
{
	Assert(filePntr != nullptr);
	Assert(filePntr->isOpen);
	Assert(filePntr->handle != INVALID_HANDLE_VALUE);
	
	if (newDataSize == 0) return true;
	Assert(newData != nullptr);
	
	DWORD bytesWritten = 0;
	if (WriteFile(filePntr->handle, newData, newDataSize, &bytesWritten, 0))
	{
		// NOTE: File read successfully
		if (bytesWritten == newDataSize)
		{
			return true;
		}
		else
		{
			DEBUG_PrintLine("Not all bytes appended to file. %u/%u written", bytesWritten, newDataSize);
			return false;
		}
	}
	else
	{
		DEBUG_PrintLine("WriteFile failed. %u/%u written", bytesWritten, newDataSize);
		return false;
	}
}

void Win32_CloseFile(OpenFile_t* filePntr)
{
	if (filePntr == nullptr) return;
	if (filePntr->handle == INVALID_HANDLE_VALUE) return;
	if (filePntr->isOpen == false) return;
	
	CloseHandle(filePntr->handle);
	filePntr->handle = INVALID_HANDLE_VALUE;
	filePntr->isOpen = false;
}

