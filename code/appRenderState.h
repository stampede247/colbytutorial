/*
File:   appRenderState.h
Author: Taylor Robbins
Date:   10\22\2017
*/

#ifndef _APP_RENDER_STATE_H
#define _APP_RENDER_STATE_H

struct RenderState_t
{
	Shader_t* boundShader;
	Texture_t* boundTexture;
	VertexBuffer_t* boundVertexBuffer;
	
	Matrix4_t worldMatrix;
	Matrix4_t viewMatrix;
	Matrix4_t projectionMatrix;
};

#endif //  _APP_RENDER_STATE_H
