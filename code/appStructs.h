/*
File:   appStructs.h
Author: Taylor Robbins
Date:   09\29\2017

#included from app.cpp
*/

#ifndef _APP_STRUCTS_H
#define _APP_STRUCTS_H


// +--------------------------------------------------------------+
// |                       Shader Variables                       |
// +--------------------------------------------------------------+
struct Shader_t
{
	GLuint id;
	
	struct
	{
		GLint position;
		GLint color;
		GLint texCoord;
	} attributes;
	
	struct
	{
		GLint worldMatrix;         //mat4
		GLint viewMatrix;          //mat4
		GLint projectionMatrix;    //mat4
		
		GLint sourceRectangle;     //vec4 (rectangle)
		
		GLint diffuseColor;        //vec4 (color)
		GLint secondaryColor;      //vec4 (color)
		
		GLint diffuseTexture;      //sampler2D
		GLint alphaTexture;        //sampler2D
		
		GLint doGrayscaleGradient; //bool
		GLint useAlphaTexture;     //bool
		
		GLint circleRadius;        //float
		GLint circleInnerRadius;   //float
	} locations;
};

struct Texture_t
{
	GLuint id;
	
	i32 width, height;
};

struct Vertex_t
{
	union
	{
		v3 position;
		struct { r32 x, y, z; };
	};
	union
	{
		v4 color;
		struct { r32 r, g, b, a; };
	};
	union
	{
		v2 texCoord;
		struct { r32 tX, tY; };
	};
};

struct VertexArray_t
{
	GLuint id;
};

struct VertexBuffer_t
{
	GLuint id;
	u32 numVertices;
};

#endif //  _APP_STRUCTS_H
