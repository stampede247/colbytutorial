/*
File:   appRenderState.cpp
Author: Taylor Robbins
Date:   10\22\2017
Description: 
	** Holds all the functions that act using the RenderState_t structure 
*/

void InitializeRenderState(RenderState_t* rs)
{
	ClearPointer(rs);
	
	rs->worldMatrix      = Matrix4_Identity;
	rs->viewMatrix       = Matrix4_Identity;
	rs->projectionMatrix = Matrix4_Identity;
}

void UpdateVAO(RenderState_t* rs)
{
	glEnableVertexAttribArray(rs->boundShader->attributes.position);
	glEnableVertexAttribArray(rs->boundShader->attributes.color);
	glEnableVertexAttribArray(rs->boundShader->attributes.texCoord);
	
	Vertex_t exampleVert = {};
	glVertexAttribPointer(rs->boundShader->attributes.position, sizeof(exampleVert.position)/sizeof(float), GL_FLOAT, false, sizeof(Vertex_t), (void*)(((u8*)&exampleVert.position) - ((u8*)&exampleVert)));
	glVertexAttribPointer(rs->boundShader->attributes.color,    sizeof(exampleVert.color)   /sizeof(float), GL_FLOAT, false, sizeof(Vertex_t), (void*)(((u8*)&exampleVert.color)    - ((u8*)&exampleVert)));
	glVertexAttribPointer(rs->boundShader->attributes.texCoord, sizeof(exampleVert.texCoord)/sizeof(float), GL_FLOAT, false, sizeof(Vertex_t), (void*)(((u8*)&exampleVert.texCoord) - ((u8*)&exampleVert)));
}

void BindShader(RenderState_t* rs, Shader_t* shaderPntr)
{
	rs->boundShader = shaderPntr;
	
	glUseProgram(shaderPntr->id);
	
	glUniform4f(rs->boundShader->locations.sourceRectangle,     0.0f, 0.0f, 1.0f, 1.0f);
	glUniform4f(rs->boundShader->locations.diffuseColor,        1.0f, 1.0f, 1.0f, 1.0f);
	glUniform4f(rs->boundShader->locations.secondaryColor,      1.0f, 1.0f, 1.0f, 1.0f);
	glUniform1i(rs->boundShader->locations.doGrayscaleGradient, false);
	glUniform1i(rs->boundShader->locations.useAlphaTexture,     false);
	glUniform1f(rs->boundShader->locations.circleRadius,        0.0f);
	glUniform1f(rs->boundShader->locations.circleInnerRadius,   0.0f);
}

void BindTexture(RenderState_t* rs, Texture_t* texturePntr)
{
	rs->boundTexture = texturePntr;
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texturePntr->id);
	glUniform1i(rs->boundShader->locations.diffuseTexture, 0);
}

void BindVertexBuffer(RenderState_t* rs, VertexBuffer_t* vertexBufferPntr)
{
	rs->boundVertexBuffer = vertexBufferPntr;
	
	glBindVertexArray(app->vertArray.id);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferPntr->id);
	UpdateVAO(rs);
}

void SetWorldMatrix(RenderState_t* rs, Matrix4_t matrix)
{
	rs->worldMatrix = matrix;
	glUniformMatrix4fv(rs->boundShader->locations.worldMatrix, 1, GL_FALSE, &matrix.values[0][0]);
}

void SetViewMatrix(RenderState_t* rs, Matrix4_t matrix)
{
	rs->viewMatrix = matrix;
	glUniformMatrix4fv(rs->boundShader->locations.viewMatrix, 1, GL_FALSE, &matrix.values[0][0]);
}

void SetProjectionMatrix(RenderState_t* rs, Matrix4_t matrix)
{
	rs->projectionMatrix = matrix;
	glUniformMatrix4fv(rs->boundShader->locations.projectionMatrix, 1, GL_FALSE, &matrix.values[0][0]);
}

// +--------------------------------------------------------------+
// |                      Drawing Functions                       |
// +--------------------------------------------------------------+

void DrawSprite(RenderState_t* rs, rec drawRec, Color_t drawColor)
{
	v4 colorVec4 = ColorToVec4(drawColor);
	glUniform4f(rs->boundShader->locations.diffuseColor, colorVec4.r, colorVec4.g, colorVec4.b, colorVec4.a);
	
	Matrix4_t worldMatrix = Matrix4Multiply(Matrix4Translate(NewVec3(drawRec.x, drawRec.y, 0)), Matrix4Scale(NewVec3(drawRec.width, drawRec.height, 1)));
	SetWorldMatrix(rs, worldMatrix);
	
	glDrawArrays(GL_TRIANGLES, 0, rs->boundVertexBuffer->numVertices);
}

void DrawCircle(RenderState_t* rs, v2 center, r32 radius, Color_t drawColor)
{
	Texture_t* oldTexture = rs->boundTexture;
	BindTexture(rs, &app->dotTexture);
	glUniform1f(rs->boundShader->locations.circleRadius,        1.0f);
	glUniform1f(rs->boundShader->locations.circleInnerRadius,   0.0f);
	
	DrawSprite(rs, NewRectangle(center - NewVec2(radius, radius), NewVec2(radius*2, radius*2)), drawColor);
	
	glUniform1f(rs->boundShader->locations.circleRadius,        0.0f);
	glUniform1f(rs->boundShader->locations.circleInnerRadius,   0.0f);
	BindTexture(rs, oldTexture);
}