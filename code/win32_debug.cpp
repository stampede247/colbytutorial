/*
File:   debugOutput.cpp
Author: Taylor Robbins
Date:   09\29\2017
Description: 
	** Holds some functions that handle outputting debug data from the program to the debuggger or console window 
*/

#define DEBUG_PRINT_BUFFER_SIZE 2048

#define DEBUG_OUTPUT_ENABLED true
#define INFO_OUTPUT_ENABLED  true
#define WARN_OUTPUT_ENABLED  true
#define ALERT_OUTPUT_ENABLED true

#define OUTPUT_FUNCTION_NAME false
#define OUTPUT_FILE_NAME     false
#define OUTPUT_LEVEL_PREFIX  true

typedef enum 
{
	OutputLevel_None   = 0x00,
	OutputLevel_Debug, //0x01
	OutputLevel_Info,  //0x02
	OutputLevel_Warn,  //0x03
	OutputLevel_Alert, //0x04
} OutputLevel_t;

static bool DebugJustWroteNewLine = true;

void Win32_Write(const char* functionName, const char* filename, OutputLevel_t outputLevel, const char* message)
{
	u32 stringLength = (u32)strlen(message);
	for (u32 cIndex = 0; cIndex < stringLength; cIndex++)
	{
		if (DebugJustWroteNewLine)
		{
			#if OUTPUT_FILE_NAME
			OutputDebugStringA(filename); OutputDebugStringA(": ");
			printf(filename); printf(": ");
			#endif
			#if OUTPUT_FUNCTION_NAME
			OutputDebugStringA(functionName); OutputDebugStringA(": ");
			printf(functionName); printf(": ");
			#endif
			#if OUTPUT_LEVEL_PREFIX
			switch (outputLevel)
			{
				case OutputLevel_Debug: OutputDebugStringA("[D] "); printf("[D] "); break;
				case OutputLevel_Info:  OutputDebugStringA("[ ] "); printf("[ ] "); break;
				case OutputLevel_Warn:  OutputDebugStringA("[W] "); printf("[W] "); break;
				case OutputLevel_Alert: OutputDebugStringA("[!] "); printf("[!] "); break;
			};
			#endif
			DebugJustWroteNewLine = false;
		}
		
		char c = message[cIndex];
		char str[2] = { c, '\0' };
		OutputDebugStringA(str);
		printf(str);
		
		if (c == '\n')
		{
			DebugJustWroteNewLine = true;
		}
	}
}
#define Win32_WriteLine(functionName, filename, outputLevel, message) do \
{                                                                        \
	Win32_Write(functionName, filename, outputLevel, message);           \
	Win32_Write(functionName, filename, outputLevel, "\n");              \
} while (0)

void Win32_Print(const char* functionName, const char* filename, OutputLevel_t outputLevel, const char* formatString, ...)
{
	char printBuffer[DEBUG_PRINT_BUFFER_SIZE];
	va_list args;
	
	va_start(args, formatString);
	size_t length = vsnprintf(printBuffer, DEBUG_PRINT_BUFFER_SIZE, formatString, args);
	va_end(args);
	
	if (length == 0)
	{
		
	}
	else if (length < DEBUG_PRINT_BUFFER_SIZE)
	{
		printBuffer[length] = '\0';
		Win32_Write(functionName, filename, outputLevel, printBuffer);
	}
	else
	{
		Win32_Write(functionName, filename, OutputLevel_Alert, "[DEBUG PRINT BUFFER OVERFLOW]");
	}
}
#define Win32_PrintLine(functionName, filename, outputLevel, formatString, ...) do \
{                                                                                  \
	Win32_Print(functionName, filename, outputLevel, formatString, ##__VA_ARGS__); \
	Win32_Write(functionName, filename, outputLevel, "\n");                        \
} while (0)

#if DEBUG_OUTPUT_ENABLED
	#define DEBUG_Write(message)               Win32_Write    (__func__, __FILE__, OutputLevel_Debug, message)
	#define DEBUG_WriteLine(message)           Win32_WriteLine(__func__, __FILE__, OutputLevel_Debug, message)
	#define DEBUG_Print(formatString, ...)     Win32_Print    (__func__, __FILE__, OutputLevel_Debug, formatString, ##__VA_ARGS__)
	#define DEBUG_PrintLine(formatString, ...) Win32_PrintLine(__func__, __FILE__, OutputLevel_Debug, formatString, ##__VA_ARGS__)
#endif
#if INFO_OUTPUT_ENABLED
	#define INFO_Write(message)                Win32_Write    (__func__, __FILE__, OutputLevel_Info, message)
	#define INFO_WriteLine(message)            Win32_WriteLine(__func__, __FILE__, OutputLevel_Info, message)
	#define INFO_Print(formatString, ...)      Win32_Print    (__func__, __FILE__, OutputLevel_Info, formatString, ##__VA_ARGS__)
	#define INFO_PrintLine(formatString, ...)  Win32_PrintLine(__func__, __FILE__, OutputLevel_Info, formatString, ##__VA_ARGS__)
#endif
#if WARN_OUTPUT_ENABLED
	#define WARN_Write(message)                Win32_Write    (__func__, __FILE__, OutputLevel_Warn, message)
	#define WARN_WriteLine(message)            Win32_WriteLine(__func__, __FILE__, OutputLevel_Warn, message)
	#define WARN_Print(formatString, ...)      Win32_Print    (__func__, __FILE__, OutputLevel_Warn, formatString, ##__VA_ARGS__)
	#define WARN_PrintLine(formatString, ...)  Win32_PrintLine(__func__, __FILE__, OutputLevel_Warn, formatString, ##__VA_ARGS__)
#endif
#if ALERT_OUTPUT_ENABLED
	#define ALERT_Write(message)               Win32_Write    (__func__, __FILE__, OutputLevel_Alert, message)
	#define ALERT_WriteLine(message)           Win32_WriteLine(__func__, __FILE__, OutputLevel_Alert, message)
	#define ALERT_Print(formatString, ...)     Win32_Print    (__func__, __FILE__, OutputLevel_Alert, formatString, ##__VA_ARGS__)
	#define ALERT_PrintLine(formatString, ...) Win32_PrintLine(__func__, __FILE__, OutputLevel_Alert, formatString, ##__VA_ARGS__)
#endif
