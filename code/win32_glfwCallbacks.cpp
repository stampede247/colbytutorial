/*
File:   win32_glfwCallbacks.cpp
Author: Taylor Robbins
Date:   09\29\2017
Description: 
	** Holds all of the callback functions that we register with GLFW 
*/

void GlfwWindowCloseCallback(GLFWwindow* window)
{
	ALERT_WriteLine("Window Closing");
}

void GlfwWindowSizeCallback(GLFWwindow* window, i32 screenWidth, i32 screenHeight)
{
	// DEBUG_PrintLine("Window Resized: %dx%d", screenWidth, screenHeight);
}

void GlfwWindowMoveCallback(GLFWwindow* window, i32 posX, i32 posY)
{
	// DEBUG_PrintLine("Window Moved: (%d, %d)", posX, posY);
}

void GlfwWindowMinimizeCallback(GLFWwindow* window, i32 isMinimized)
{
	// DEBUG_PrintLine("Window %s", (isMinimized > 0) ? "Minimized" : "Re-Opened");
}

void GlfwKeyPressedCallback(GLFWwindow* window, i32 key, i32 scanCode, i32 action, i32 modifiers)
{
	//TODO: Handle repeated keys?
	if (action == GLFW_REPEAT) { return; }
	HandleKeyEvent(window, currentAppInput, key, action == GLFW_PRESS);
}

void GlfwCharPressedCallback(GLFWwindow* window, u32 codepoint)
{
	// DEBUG_PrintLine("Text Input: 0x%X", codepoint);
	if (currentAppInput->textInputLength < ArrayCount(currentAppInput->textInput))
	{
		currentAppInput->textInput[currentAppInput->textInputLength] = (u8)codepoint;
		currentAppInput->textInputLength++;
	}
}

void GlfwCursorPosCallback(GLFWwindow* window, real64 mouseX, real64 mouseY)
{
	currentAppInput->mousePos = NewVec2((r32)mouseX, (r32)mouseY);
	
	if (currentAppInput->buttons[MouseButton_Left].isDown)
	{
		r32 distance = Vec2Length(currentAppInput->mousePos - currentAppInput->mouseStartPos[MouseButton_Left]);
		if (distance > currentAppInput->mouseMaxDist[MouseButton_Left])
		{
			currentAppInput->mouseMaxDist[MouseButton_Left] = distance;
		}
	}
	if (currentAppInput->buttons[MouseButton_Right].isDown)
	{
		r32 distance = Vec2Length(currentAppInput->mousePos - currentAppInput->mouseStartPos[MouseButton_Right]);
		if (distance > currentAppInput->mouseMaxDist[MouseButton_Right])
		{
			currentAppInput->mouseMaxDist[MouseButton_Right] = distance;
		}
	}
	if (currentAppInput->buttons[MouseButton_Middle].isDown)
	{
		r32 distance = Vec2Length(currentAppInput->mousePos - currentAppInput->mouseStartPos[MouseButton_Middle]);
		if (distance > currentAppInput->mouseMaxDist[MouseButton_Middle])
		{
			currentAppInput->mouseMaxDist[MouseButton_Middle] = distance;
		}
	}
}

void GlfwMousePressCallback(GLFWwindow* window, i32 button, i32 action, i32 modifiers)
{
	//TODO: Handle repeated keys?
	if (action == GLFW_REPEAT) { return; }
	
	HandleMouseEvent(window, currentAppInput, button, action == GLFW_PRESS);
}

void GlfwMouseScrollCallback(GLFWwindow* window, real64 deltaX, real64 deltaY)
{
	currentAppInput->scrollValue += NewVec2((r32)deltaX, (r32)deltaY);
	currentAppInput->scrollDelta += NewVec2((r32)deltaX, (r32)deltaY);
}
