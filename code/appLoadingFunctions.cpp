/*
File:   app_loadingFunctions.cpp
Author: Taylor Robbins
Date:   10\22\2017
Description: 
	** Holds all the wrapper functions for loading various resources (like textures and shaders) 
*/


// +==============================+
// |         Load Shaders         |
// +==============================+
Shader_t LoadShader(const char* vertexShaderFilePath, const char* fragmentShaderFilePath)
{
	Shader_t result = {};
	FileInfo_t vertexShaderFile = Win32_ReadEntireFile(vertexShaderFilePath);
	FileInfo_t fragmentShaderFile = Win32_ReadEntireFile(fragmentShaderFilePath);
	
	DEBUG_PrintLine("Vertex shader: \"%s\"", (const char*)vertexShaderFile.content);
	INFO_WriteLine("");
	DEBUG_PrintLine("Fragment shader: \"%s\"", (const char*)fragmentShaderFile.content);
	
	GLint compilationResult;
	
	DEBUG_WriteLine("Compiling vertex shader");
	GLuint vertShaderId = glCreateShader(GL_VERTEX_SHADER); 
 	
	glShaderSource(vertShaderId, 1, (const char* const*)&vertexShaderFile.content, NULL);
	glCompileShader(vertShaderId);
	
	glGetShaderiv(vertShaderId, GL_COMPILE_STATUS, &compilationResult);
	if (compilationResult == GL_FALSE)
	{
		ALERT_PrintLine("Vertex shader compilation failed");
		Assert(false);
	}
	
	DEBUG_WriteLine("Compiling fragment shader");
	GLuint fragShaderId = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragShaderId, 1, (const char* const*)&fragmentShaderFile.content, NULL);
	glCompileShader(fragShaderId);
	
	glGetShaderiv(fragShaderId, GL_COMPILE_STATUS, &compilationResult);
	if (compilationResult == GL_FALSE)
	{
		ALERT_PrintLine("Fragment shader compilation failed");
		Assert(false);
	}
	
	Win32_FreeFileMemory(&vertexShaderFile);
	Win32_FreeFileMemory(&fragmentShaderFile);
	
	DEBUG_WriteLine("Linking shader");
	result.id = glCreateProgram();
	glAttachShader(result.id, vertShaderId);
	glAttachShader(result.id, fragShaderId);
	glLinkProgram(result.id);
	
	glGetProgramiv(result.id, GL_LINK_STATUS, &compilationResult);
	
	if (compilationResult == GL_FALSE)
	{
		ALERT_WriteLine("Shader linking failed");
		Assert(false);
	}
	
	result.attributes.position = glGetAttribLocation(result.id, "inPosition");
	result.attributes.color    = glGetAttribLocation(result.id, "inColor");
	result.attributes.texCoord = glGetAttribLocation(result.id, "inTexCoord");
	
	result.locations.worldMatrix         = glGetUniformLocation(result.id, "WorldMatrix");
	result.locations.viewMatrix          = glGetUniformLocation(result.id, "ViewMatrix");
	result.locations.projectionMatrix    = glGetUniformLocation(result.id, "ProjectionMatrix");
	result.locations.sourceRectangle     = glGetUniformLocation(result.id, "SourceRectangle");
	result.locations.diffuseColor        = glGetUniformLocation(result.id, "DiffuseColor");
	result.locations.secondaryColor      = glGetUniformLocation(result.id, "SecondaryColor");
	result.locations.diffuseTexture      = glGetUniformLocation(result.id, "DiffuseTexture");
	result.locations.alphaTexture        = glGetUniformLocation(result.id, "AlphaTexture");
	result.locations.doGrayscaleGradient = glGetUniformLocation(result.id, "DoGrayscaleGradient");
	result.locations.useAlphaTexture     = glGetUniformLocation(result.id, "UseAlphaTexture");
	result.locations.circleRadius        = glGetUniformLocation(result.id, "CircleRadius");
	result.locations.circleInnerRadius   = glGetUniformLocation(result.id, "CircleInnerRadius");
	
	return result;
}

Texture_t CreateTexture(u8* textureData, i32 width, i32 height)
{
	Texture_t result = {};
	result.width = width;
	result.height = height;
	
	glGenTextures(1, &result.id);
	glBindTexture(GL_TEXTURE_2D, result.id);
	
	glTexImage2D(
		GL_TEXTURE_2D,
		0,
		GL_RGBA,
		width, height,
		0,
		GL_RGBA,
		GL_UNSIGNED_BYTE,
		textureData
	);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); 
	glGenerateMipmap(GL_TEXTURE_2D);
	
	return result;
}

Texture_t LoadTexture(const char* filename)
{
	Texture_t result = {};
	
	FileInfo_t textureFile = Win32_ReadEntireFile(filename);
	
	i32 numChannels;
	i32 width, height;
	u8* imageData = stbi_load_from_memory(
		(u8*)textureFile.content, textureFile.size,
		&width, &height, &numChannels, 4
	);
	
	DEBUG_PrintLine("Image is %dx%d pixels", width, height);
	
	result = CreateTexture(imageData, width, height);
	
	stbi_image_free(imageData);
	Win32_FreeFileMemory(&textureFile);
	
	return result;
}

VertexArray_t CreateVertexArray()
{
	VertexArray_t result = {};
	
	glGenVertexArrays(1, &result.id);
	glBindVertexArray(result.id);
	
	return result;
}

VertexBuffer_t CreateVertexBuffer(const Vertex_t* vertices, u32 numVertices)
{
	VertexBuffer_t result = {};
	result.numVertices = numVertices;
	
	glGenBuffers(1, &result.id);
	glBindBuffer(GL_ARRAY_BUFFER, result.id);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex_t) * numVertices, vertices, GL_STATIC_DRAW);
	
	return result;
}


